#!/usr/bin/env bash
#
# (C) Do Le Quoc, 2014
#

sudo apt-get install python-pip
sudo pip install flask
sudo pip install ujson
